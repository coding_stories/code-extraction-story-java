# Code extraction
**To read**: [https://gitlab.com/coding_stories/code-extraction-story-java]

**Estimated reading time**: 10 minutes

## Story Outline
This story is about extracting code into simpler methods to make it more readable

## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #clean_code, #code_extraction